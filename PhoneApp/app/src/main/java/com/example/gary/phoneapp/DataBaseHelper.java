package com.example.gary.phoneapp;

/**
 * Created by Gary on 24/5/15.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "People.db";
    public static final String TABLE_NAME = "contacts_table";
    public static final String COL_1 = "firstname";
    public static final String COL_2 = "lastname";
    public static final String COL_3 = "mobile";
    public static final String COL_4 = "office";
    public static final String COL_5 = "email";
    public static final String COL_6 = "dob";
    public static final String COL_7 = "gender";

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NAME + " (firstname TEXT, lastname TEXT,mobile TEXT,office TEXT, email TEXT, dob TEXT, gender INTEGER)");
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);
    }

    public boolean insertData(String fn,String ln,String mobile,String office,String email,String dob,Boolean ismale) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1,fn);
        contentValues.put(COL_2,ln);
        contentValues.put(COL_3,mobile);
        contentValues.put(COL_4, office);
        contentValues.put(COL_5, email);
        contentValues.put(COL_6, dob);
        contentValues.put(COL_7, ismale);
        long result = db.insert(TABLE_NAME,null ,contentValues);
        if(result == -1)
            return false;
        else
            return true;
    }
    public void deleteData(String fn){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_NAME + " WHERE " + COL_1 + " = " + fn);

    }

    public Cursor getAllData(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from " +TABLE_NAME, null);
        return res;
    }
}
