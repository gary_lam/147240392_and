package com.example.gary.phoneapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class ListActivity extends Activity {
    ListView lv;
    TextView con_info;
    DataBaseHelper myDb;

    ArrayAdapter<String> dbres;
    Integer i =0; //res Cursor Value
    String delholder;

    //Fetch result and register contextMenu to the list View
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        myDb = new DataBaseHelper(this);
        Cursor res = myDb.getAllData();
        String[] NAMES = new String[res.getCount()];
        lv = (ListView)findViewById(R.id.smt_list);
        con_info = (TextView) findViewById(R.id.tv_info);
        registerForContextMenu(lv);

       //Handling for  the fetched result
       while (res.moveToNext()) {
           if(res.getInt(6)== 1){
               NAMES[i] = String.format("%s  %-20s %s", "( M )",res.getString(0), res.getString(2));
           }
           else {
               NAMES[i] = String.format("%s %-20s %s", "( F )",res.getString(0), res.getString(2));
           }
            i++;
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.list_row,NAMES);
        lv.setAdapter(adapter);
        show();

    }
    //Dial the number if any saved contacts is clicked
    public void show(){
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String temp_phoneNumber = parent.getItemAtPosition(position).toString();
                String phoneNumber = temp_phoneNumber.substring((temp_phoneNumber.trim().length()) - 8);
                Intent intentDial = new Intent("android.intent.action.CALL", Uri.parse("tel:" + phoneNumber));
                startActivity(intentDial);



            }

        });
    }

    // No Options Menu in this Activity
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    // No Options Menu in this Activity
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_showall) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_list, menu);
    }

    //Change activity if user pressed addnew
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_addnew) {

            Intent intent = new Intent (this, MainActivity.class);
            startActivity(intent);

            return true;
        }
        else{
            Toast.makeText(ListActivity.this, item.getTitle() + " Clicked", Toast.LENGTH_SHORT).show();
        }

        return super.onContextItemSelected(item);

    }
}
