package com.example.gary.phoneapp;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {
    EditText et_firstname, et_lastname, et_mobile, et_office, et_email, et_dob;
    Button btn_dob, btn_save, btn_load;
    Switch sw_ismale;
    TextView tv_gender;
    DataBaseHelper myDb;
    Intent intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Database Connection Object
        myDb = new DataBaseHelper(this);
        //UI Components Initialization
        btn_save = (Button) findViewById(R.id.btn_save);
        btn_load = (Button) findViewById(R.id.btn_load);
        et_firstname = (EditText) findViewById(R.id.et_firstname);
        et_lastname = (EditText) findViewById(R.id.et_lastname);
        et_mobile = (EditText) findViewById(R.id.et_mobile);
        et_office = (EditText) findViewById(R.id.et_office);
        et_email = (EditText) findViewById(R.id.et_email);
        et_dob = (EditText) findViewById(R.id.et_dob);
        sw_ismale = (Switch) findViewById(R.id.ismale);
        tv_gender = (TextView) findViewById(R.id.tv_gender);
        //EventListener and relative method declaration
        intent = new Intent(this, ListActivity.class);
        AddData();
        viewAll();
        changegender();


    }

    // UI Components && Listener Implementation
    // Change the selected gender by the switch object(sw_gender)and show it by using a textview object(tv_gender)
    public void changegender() {
        sw_ismale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    tv_gender.setText("Male");
                } else {
                    tv_gender.setText("Female");
                }

            }
        });
    }

    // UI Components && Listener Implementation
    // Trigger the DatePickerDialog to choose the DOB for fill
    public void btn_dob(View view) {
        DatePickerDialogFragment dlg = DatePickerDialogFragment.newInstance();
        FragmentManager fm = getSupportFragmentManager();
        dlg.show(fm, "datepickerdialog");
    }

    // Get the value from the DatePickerDialog then pass it to the EditTextObject(et_dob)
    public void getDate(int year,
                        int monthOfYear, int dayOfMonth) {
        et_dob.setText(Integer.toString(year) + "/" +
                Integer.toString(monthOfYear + 1) + "/" +
                Integer.toString(dayOfMonth));
    }

    // Add all filled data to SQLite db and show SUCCESS/FAILURE notification by using ToastObject
    public void AddData() {
        btn_save.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Map all filled data in the EditTextObject to the DataBase
                        save();
                    }
                }
        );
    }
    // Save records from view to SQLite
    public void save() {
        boolean isInserted = myDb.insertData(
                et_firstname.getText().toString(),
                et_lastname.getText().toString(),
                et_mobile.getText().toString(),
                et_office.getText().toString(),
                et_email.getText().toString(),
                et_dob.getText().toString(),
                sw_ismale.isChecked());
        // Show SUCCESSFUL/FAILURE after the SQL statement runs by using ToastObject
        if (isInserted = true) {
            showMessage("Insert Data", "Successful");
            //Toast.makeText(MainActivity.this, "Data Inserted", Toast.LENGTH_LONG).show();
        } else
            Toast.makeText(MainActivity.this, "Data not Inserted", Toast.LENGTH_LONG).show();

    }

    // Retrieve the records from SQLite and try to show all records in a AlertDialogObject
    public void viewAll() {
        btn_load.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Cursor res = myDb.getAllData();
                        // Show Error Message to Client if Nothing was found in the database
                        if (res.getCount() == 0) {
                            showMessage("Error", "No Data Inside");
                            return;
                        }
                        // Gather records from SQLite
                        StringBuffer buffer = new StringBuffer();
                        while (res.moveToNext()) {
                            buffer.append("FN: " + res.getString(0) + "\n");
                            buffer.append("LN: " + res.getString(1) + "\n");
                            buffer.append("N1: " + res.getString(2) + "\n");
                            buffer.append("N2: " + res.getString(3) + "\n");
                            buffer.append("EM: " + res.getString(4) + "\n");
                            buffer.append("DOB: " + res.getString(5) + "\n");
                            buffer.append("GE: " + res.getString(6) + "\n\n");
                        }
                        //Show all records in a AlertDialogObject
                        showMessage("Result", buffer.toString());


                    }
                }
        );
    }

    //Inner class of handling Message appears
    public void showMessage(String title, String Message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //handling action to specify menu items
        switch (id) {
            //Start another Activity if user select ShowAll
            case R.id.action_showall:
                startActivity(intent);
                break;

            //Save and start another Activity if user select ShowAll
            case R.id.action_save:
                save();
                startActivity(intent);
                break;

        }

        return super.onOptionsItemSelected(item);
    }
}

